# mandlebrot

## Usage
```
$ ./mandelbrot <image name> <resolution> <complx number upper left> <complx number lower right>
```

### Usage example
```
$ ./mandelbrot output.png 6000x4000 -0.5,0.65 -1,0.2
```
### Output
![Sample Output](https://github.com/manank20/mandlebrot/blob/master/sample_outputs/outpt.png)
